package com.devcamp.c80.musicmodelsapi.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.devcamp.c80.musicmodelsapi.models.Album;

@Service
public class AlbumService {
    
    public List<String> createSongs1() {
        List<String> songs = new ArrayList<>();
        songs.add("Song 1");
        songs.add("Song 2");
        songs.add("Song 3");
        return songs;
    }

    Album album1 = new Album("Album1", createSongs1());
    Album album2 = new Album("Album2", createSongs1());
    Album album3 = new Album("Album3", createSongs1());
    Album album4 = new Album("Album4", createSongs1());
    Album album5 = new Album("Album5", createSongs1());
    Album album6 = new Album("Album6", createSongs1());
    Album album7 = new Album("Album7", createSongs1());
    Album album8 = new Album("Album8", createSongs1());
    Album album9 = new Album("Album9", createSongs1());
    Album album10 = new Album("Album10", createSongs1());

    public ArrayList<Album> getAllAlbums() {
        ArrayList<Album> allAlbum = new ArrayList<Album>();

        allAlbum.add(album1);
        allAlbum.add(album2);
        allAlbum.add(album3);
        allAlbum.add(album4);
        allAlbum.add(album5);
        allAlbum.add(album6);
        allAlbum.add(album7);
        allAlbum.add(album8);
        allAlbum.add(album9);
        allAlbum.add(album10);

        return allAlbum;
    }

}
