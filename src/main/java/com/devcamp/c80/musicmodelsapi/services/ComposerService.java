package com.devcamp.c80.musicmodelsapi.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.c80.musicmodelsapi.models.Album;
import com.devcamp.c80.musicmodelsapi.models.Artist;
import com.devcamp.c80.musicmodelsapi.models.Band;
import com.devcamp.c80.musicmodelsapi.models.BandMember;
import com.devcamp.c80.musicmodelsapi.models.Composer;

@Service
public class ComposerService {
    @Autowired
    AlbumService albumService;

    public ArrayList<Band> getAllBands(){
        ArrayList<Band> bands = new ArrayList<>();

        List<BandMember> bandMember1 = new ArrayList<>();
        bandMember1.add(new BandMember("firstname1", "lastname1", "stagename1", "instrument1"));
        bandMember1.add(new BandMember("firstname2", "lastname2", "stagename2", "instrument2"));
        Band band1 = new Band("Band 1", bandMember1, albumService.getAllAlbums());
        bands.add(band1);
        

        List<BandMember> bandMember2 = new ArrayList<>();
        bandMember2.add(new BandMember("firstname1", "lastname1", "stagename1", "instrument1"));
        bandMember2.add(new BandMember("firstname2", "lastname2", "stagename2", "instrument2"));
        Band band2 = new Band("Band 2", bandMember1, albumService.getAllAlbums());
        bands.add(band2);

        return bands;
    }

    public ArrayList<Composer> getAllComposers() {
        ArrayList<Composer> composers = new ArrayList<>();

        // Thêm các Artist vào danh sách
        List<Album> artist1Albums = new ArrayList<>();
        artist1Albums.add(new Album("Album1", albumService.createSongs1()));
        Artist artist1 = new Artist("John", "Doe", "JohnD", artist1Albums);
        composers.add(artist1);

        List<Album> artist2Albums = new ArrayList<>();
        artist2Albums.add(new Album("Album 2", albumService.createSongs1()));
        artist2Albums.add(new Album("Album 3", albumService.createSongs1()));
        Artist artist2 = new Artist("Jane", "Smith", "JaneS", artist2Albums);
        composers.add(artist2);

        List<Album> artist3Albums = new ArrayList<>();
        artist3Albums.add(new Album("Album 4", albumService.createSongs1()));
        Artist artist3 = new Artist("David", "Johnson", "DavidJ", artist3Albums);
        composers.add(artist3);

        // Thêm các BandMember vào danh sách
        BandMember bandMember1 = new BandMember("Mike", "Brown", "MikeB", "Guitar");
        composers.add(bandMember1);

        BandMember bandMember2 = new BandMember("Lisa", "Taylor", "LisaT", "Bass");
        composers.add(bandMember2);

        BandMember bandMember3 = new BandMember("Tom", "Wilson", "TomW", "Drums");
        composers.add(bandMember3);

        BandMember bandMember4 = new BandMember("Sarah", "Clark", "SarahC", "Keyboard");
        composers.add(bandMember4);

        BandMember bandMember5 = new BandMember("Mark", "Anderson", "MarkA", "Saxophone");
        composers.add(bandMember5);

        BandMember bandMember6 = new BandMember("Emily", "Robinson", "EmilyR", "Trumpet");
        composers.add(bandMember6);

        return composers;
    }

    public ArrayList<Artist> getAllArtist(){
        ArrayList<Artist> allArtist = new ArrayList<>();
        
        allArtist.add(new Artist("Son", "Tung", "MTP", albumService.getAllAlbums()));
        allArtist.add(new Artist("Hoang", "Son", "SooBin", albumService.getAllAlbums()));
        allArtist.add(new Artist("Anh", "Tuan", "HAT", albumService.getAllAlbums()));

        return allArtist;
    }
}
