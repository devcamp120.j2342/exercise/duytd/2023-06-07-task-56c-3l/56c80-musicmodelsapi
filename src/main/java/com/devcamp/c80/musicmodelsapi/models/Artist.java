package com.devcamp.c80.musicmodelsapi.models;

import java.util.List;

public class Artist extends Composer{
    private List<Album> album;

    public Artist(String firstname, String lastname, String stagename, List<Album> album) {
        super(firstname, lastname, stagename);
        this.album = album;
    }

    public List<Album> getAlbum() {
        return album;
    }

    public void setAlbum(List<Album> album) {
        this.album = album;
    }

    @Override
    public String toString() {
        return "Artist [album=" + album + "]";
    }

    
    
}
