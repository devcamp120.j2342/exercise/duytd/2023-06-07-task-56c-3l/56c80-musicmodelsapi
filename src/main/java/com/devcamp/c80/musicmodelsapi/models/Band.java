package com.devcamp.c80.musicmodelsapi.models;

import java.util.List;

public class Band {
    private String bandname;
    private List<BandMember> menbers;
    private List<Album> albums;
    
    public Band(String bandname, List<BandMember> menbers, List<Album> albums) {
        this.bandname = bandname;
        this.menbers = menbers;
        this.albums = albums;
    }

    public String getBandname() {
        return bandname;
    }

    public void setBandname(String bandname) {
        this.bandname = bandname;
    }

    public List<BandMember> getMenbers() {
        return menbers;
    }

    public void setMenbers(List<BandMember> menbers) {
        this.menbers = menbers;
    }

    public List<Album> getAlbums() {
        return albums;
    }

    public void setAlbums(List<Album> albums) {
        this.albums = albums;
    }

    @Override
    public String toString() {
        return "Band [bandname=" + bandname + ", menbers=" + menbers + ", albums=" + albums + "]";
    }

    
}
