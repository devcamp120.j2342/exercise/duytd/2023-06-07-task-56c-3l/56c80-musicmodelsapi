package com.devcamp.c80.musicmodelsapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.c80.musicmodelsapi.models.Artist;
import com.devcamp.c80.musicmodelsapi.models.Band;
import com.devcamp.c80.musicmodelsapi.services.ComposerService;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class ComposerController {
    @Autowired
    ComposerService composerService;

    @GetMapping("/bands")
    public ArrayList<Band> getAllBands(){
        ArrayList<Band> bands = composerService.getAllBands();
        return bands;
    }

    @GetMapping("/artist")
    public ArrayList<Artist> getAllArtist(){
        ArrayList<Artist> artist = composerService.getAllArtist();
        return artist;
    }
}
