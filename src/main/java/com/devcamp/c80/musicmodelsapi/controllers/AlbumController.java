package com.devcamp.c80.musicmodelsapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.c80.musicmodelsapi.models.Album;
import com.devcamp.c80.musicmodelsapi.services.AlbumService;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class AlbumController {
    @Autowired
    AlbumService albumService;

    @GetMapping("/albums")
    public ArrayList<Album> getAllAlbums(){
        ArrayList<Album> allAlbum = albumService.getAllAlbums();
        return allAlbum;
    }
}
